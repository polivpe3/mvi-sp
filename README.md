

# Semestrální práce k předmětu MI-MVI.

Zadání:

Naučte Generative Adversarial Network (GAN) generovat hudbu. Vyberte si, jestli budete generovat pouze noty nebo celou audio stopu.

Spuštění:

V archivu MidiGAN.zip se nachází zdrojový kod v notebooku MidiGAN.ipynb a složka s testovacími daty pro trénování.

Pro spuštění je potřeba stáhnout knihovnu Music21 http://web.mit.edu/music21/doc/installing/installLinux.html.

Podrobné informace o projektu jsou popsány v Report.pdf.

Výsledné melodie se nachází ve složce /midi.